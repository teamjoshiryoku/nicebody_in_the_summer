﻿ /*This script is written by AyanoMasaki 20/6/2015
	scripting notes movement
 */


using UnityEngine;
using System.Collections;

public class Notes : MonoBehaviour {

	public float NotesSpeed;
	public GameObject ParticlePrefab;

	private bool moveFlag;
	private bool attackFlag;
	private Vector3 velocity;
	private AudioSource effectPow;
	private AudioSource effectJump;
	private AudioSource effectCute;

	public float animation_speed;

	void Start () {
		moveFlag   = true;
		attackFlag = false;
		velocity   = Vector3.zero;
		AudioSource[] audioSources = GetComponents<AudioSource> ();
		effectPow = audioSources[0];
		effectJump = audioSources[1];
		effectCute = audioSources [2];

		this.GetComponent<Animator> ().speed = animation_speed;

	}

	void Update () {

		if (moveFlag) {
			move ();
		}


		if (Input.GetKeyDown("j")) {
			ForwardJump ();
		}


		if (Input.GetKeyDown ("s")) {
			SideJump();
		}

		if(Input.GetKey ("a")){
			attack ();
			 
		}

		velocity.y -= 9.8f * Time.deltaTime;
		this.GetComponent<CharacterController> ().Move (velocity * Time.deltaTime);
	}


	void move(){
		this.GetComponent<Animator> ().Play ("Run");
		//this.transform.Translate (Vector3.forward * NotesSpeed * Time.deltaTime);
	}


	void ForwardJump(){
		moveFlag   = false;
		attackFlag = false;
		this.GetComponent<Animator> ().Play ("Jump_00");
		velocity.y = 7.0f;
		velocity.z = 9.0f;
		velocity.x = 2.4f;
		StartCoroutine ("Wait", 1.0f);
		effectCute.PlayOneShot(effectCute.clip);

	}

	void SideJump(){
		int rand;

		rand = Random.Range (0, 2);

		moveFlag   = false;
		attackFlag = false;
		this.GetComponent<Animator> ().Play ("Jump_01");
		velocity.y = 7.0f;
		effectJump.PlayOneShot (effectJump.clip);
		Debug.Log (rand);
		if (rand == 0) { // move left
			velocity.z = 13.0f;
		}
		else {	// move right
			velocity.x = 4.0f;
		}
		StartCoroutine ("Wait", 1.0f);
	}

	void attack(){
		moveFlag   = false;
		attackFlag = true;
		this.GetComponent<Animator> ().Play ("Attack");
		StartCoroutine ("Wait", 1.0f);
	}

	private IEnumerator Wait(float time){
		yield return new WaitForSeconds(time);
		velocity = Vector3.zero;
		moveFlag   = true;
		attackFlag = false;
	}


	void OnTriggerStay(Collider coll){
		if (coll.gameObject.tag == "rock") {
		
			if (attackFlag) {
				Instantiate(ParticlePrefab,coll.transform.position,ParticlePrefab.transform.rotation);
				moveFlag = true;
				Destroy(coll.gameObject);
				effectPow.PlayOneShot(effectPow.clip);
	
			}
			else{
				moveFlag = false;
				this.GetComponent<Animator> ().Play ("Idle");
			}
		}
		if (coll.gameObject.tag == "fin") {
			System.Threading.Thread.Sleep (1000);
			SceneLoad();
		}
	}
	public void SceneLoad(){
		Application.LoadLevel ("fin");
	} 
}
